const numberToEn = (n, isDot = false) =>
{
  let string = n.toString(), units, tens, scales, start, end, chunks, chunksLen, chunk, ints, i, word, words, and, hundred, mhun, san;

  and = 'and'
  hundred = 'hundred'
  string = string.replace(/[, ]/g, '');
  if (parseInt(string) === 0)
  {
    return 'zero';
  }
  units = [ '', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten', 'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen' ];
  tens = [ '', '', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety' ];
  scales = [ '', 'thousand', 'million', 'billion', 'trillion', 'quadrillion', 'quintillion', 'sextillion', 'septillion', 'octillion', 'nonillion', 'decillion', 'undecillion', 'duodecillion', 'tredecillion', 'quatttuor-decillion', 'quindecillion', 'sexdecillion', 'septen-decillion', 'octodecillion', 'novemdecillion', 'vigintillion', 'centillion' ];
  let sp = string.split('.')
  string = sp[0]
  const stringDot = sp[1]
  start = string.length;
  chunks = [];
  while (start > 0)
  {
    end = start;
    chunks.push(string.slice((start = Math.max(0, start - 3)), end));
  }
  chunksLen = chunks.length;
  if (chunksLen > scales.length)
  {
    return '';
  }
  words = [];
  for (i = 0; i < chunksLen; i++)
  {
    chunk = parseInt(chunks[i]);
    if (chunk)
    {
      ints = chunks[i].split('').reverse().map(parseFloat);
      if (ints[1] === 1)
      {
        ints[0] += 10;
      }
      if ((word = scales[i]))
      {
        words.push(word);
      }
      if ((word = units[ints[0]]))
      {
        words.push(word);
      }
      if ((word = tens[ints[1]]))
      {
        words.push(word);
      }
      if (ints[0] || ints[1])
      {
        if (ints[2] || !i && chunksLen)
        {
          if(!isDot)
          {
            words.push(and);
          }
        }
      }
      if ((word = units[ints[2]]))
      {
        words.push(word + hundred);
      }
    }
  }

  let result = ''
  result = `${words.reverse().join(' ')}`;
  if (stringDot)
  {
    result = `${result} dot ${numberToText(stringDot, true)}`
  }
  else
  {
    if (!isDot)
    {
      result = `${result}`
    }
  }
  result = result[0].toUpperCase() + result.substring(1).toLowerCase();

  return result
}

const numberToTh = (strNumber, IsTrillion = false) =>
{
  strNumber = strNumber.toString()

  let BahtText = ''
  let strTrillion = ''
  let strThaiNumber =  ['ศูนย์', 'หนึ่ง', 'สอง', 'สาม', 'สี่', 'ห้า', 'หก', 'เจ็ด', 'แปด', 'เก้า', 'สิบ']
  let strThaiPos = [ '', 'สิบ', 'ร้อย', 'พัน', 'หมื่น', 'แสน', 'ล้าน']

  let decNumber = 0;
  decNumber = parseFloat(strNumber).toFixed(2)
  if (decNumber === 0)
  {
    return 'ศูนย์บาท';
  }
  strNumber = decNumber.toString('0.00');
  let strInteger = strNumber.split('.')[0];
  let strSatang = strNumber.split('.')[1];
  if (strInteger.length > 13)
  {
    return ('รองรับตัวเลขได้เพียง ล้านล้าน เท่านั้น!');
  }
  let _IsTrillion = strInteger.length > 7;
  if (_IsTrillion)
  {
    strTrillion = strInteger.substring(0, strInteger.length - 6);
    BahtText = numberToTh(strTrillion, _IsTrillion);
    strInteger = strInteger.substring(strTrillion.length);
  }
  let strLength = strInteger.length;
  for (let i = 0; i < strInteger.length; i++)
  {
    let number = strInteger.substring(i, 1 + i);
    if (number !== '0')
    {
      if (i === strLength - 1 && number === '1' && strLength !== 1)
      {
        BahtText += 'เอ็ด';
      }
      else if (i === strLength - 2 && number === '2' && strLength !== 1)
      {
        BahtText += 'ยี่';
      }
      else if (i !== strLength - 2 || number !== '1')
      {
        BahtText += strThaiNumber[parseInt(number)];
      }
      BahtText += strThaiPos[(strLength - i) - 1];
    }
  }
  if (IsTrillion)
  {
    BahtText += 'ล้าน'
    return BahtText;
  }
  if (strInteger !== '0')
  {
    BahtText += 'บาท';
  }
  if (strSatang === '00')
  {
    // BahtText += 'ถ้วน';
  }
  else
  {
    strLength = strSatang.length;
    for (let i = 0; i < strSatang.length; i++)
    {
      let number = strSatang.substring(i, 1 + i);
      if (number !== '0')
      {
        if (i === strLength - 1 && number === '1' && strSatang[0].toString() !== '0')
        {
          BahtText += 'เอ็ด';
        }
        else if (i === strLength - 2 && number === '2' && strSatang[0].toString() !== '0')
        {
          BahtText += 'ยี่';
        }
        else if (i !== strLength - 2 || number !== '1')
        {
          BahtText += strThaiNumber[parseInt(number)];
        }
        BahtText += strThaiPos[(strLength - i) - 1];
      }
    }
    BahtText += 'สตางค์';
  }
  return BahtText;
}

module.exports = { numberToEn, numberToTh }
