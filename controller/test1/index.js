const express = require('express');
const router = express.Router();
const { numberToEn, numberToTh } = require('./util')

/*
Requirement:
- ใส่ธนบัตร ได้ไม่เกิน 1000 ใบ
- ใส่เหรียญ ได้ไม่เกิน 500 เหรียญ
- ใส่เหรียญ และธนบัตร รวมกันได้ไม่เกิน 1200 ใบ/เหรียญ
*/

router.get('/counter', (req, res) =>
  {
    const lang = req.headers['accept-language']
    const validateBankCount = 1000
    const validateCoinCount = 500
    const validateTotalCount = 1200
    const bankKey = ['1000', '500', '100', '50', '20']
    const coinKey = [ '10', '5', '2', '1', '0.50', '0.25']

    const validate = (values) =>
    {
      const checkIsNumber = (v) => typeof Number(v) === 'number' || v !== undefined

      let validateStatus = true;
      let countBank = 0;
      let countCoin = 0;
      bankKey.forEach(b =>
      {
        if (!checkIsNumber(values[b]))
        {
          validateStatus = false;
        }
        if (values[b] !== undefined)
        {
          countBank += Number(values[b])
        }
      });
      if (countBank > validateBankCount)
      {
        validateStatus = false;
      }
      coinKey.forEach(c =>
      {
        if (!checkIsNumber(values[c]))
        {
          validateStatus = false;
        }
        if (values[c] !== undefined)
        {
          countCoin += Number(values[c])
        }
      })
      if (countCoin > validateCoinCount)
      {
        validateStatus = false;
      }
      return (countBank + countCoin <= validateTotalCount) && validateStatus
    }

    const counting = (values) =>
    {
      let total = 0
      const keys = bankKey.concat(coinKey)
      keys.map((key) => total += Number(key) * values[key] || 1)
      return total
    }

    try
    {
      if (validate(req.query))
      {
        const total = counting(req.query)
        const currency = Number(total).toLocaleString()
        const currencyText = lang === 'th' ? numberToTh(total) : numberToEn(total)

        res.status(200).json({ total, currency, currencyText })
      }
      else
      {
        res.status(400).json({ errorMessage: 'Validate fail.' })
      }
    }
    catch (e)
    {
      res.status(400).json({ errorMessage: e.toString() })
    }
  }
);

module.exports = router
