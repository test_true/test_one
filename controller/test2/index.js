const express = require('express');
const router = express.Router();

router.post('/login', (req, res) => {
        try {
            if (req.body) {
                const {username, password} = req.body

                if (username === 'n.luengthongdee@gmail.com' && password === 'password') {
                    const status = true;
                    const message = 'Success'
                    res.status(200).json({
                        status, message, user: {
                            fullname: 'Nattapon Luengthongdee',
                            age: 31
                        }
                    })
                } else {
                    const status = false;
                    const message = 'Username or Password not correct.'
                    res.status(400).json({status, message})
                }
            } else {
                res.status(400).json({errorMessage: 'Login fail.'})
            }
        } catch (e) {
            res.status(400).json({errorMessage: e.toString()})
        }
    }
);

router.post('/sendFoods', (req, res) => {
        try {
            if (req.body) {
                console.log(req.body.foods)
                const status = true;
                const message = 'Success'
                res.status(200).json({status, message})
            } else {
                res.status(400).json({errorMessage: 'No foods data.'})
            }
        } catch (e) {
            res.status(400).json({errorMessage: e.toString()})
        }
    }
);

module.exports = router
