const express = require('express')
const app = express()
const cors = require("cors");
const test1Router = require('./controller/test1');
const test2Router = require('./controller/test2');

app.use(express.json());

app.use(cors())

app.use('/test1', test1Router);
app.use('/test2', test2Router);

app.listen(3001, () =>
{
  console.log('Start server at port http://localhost:3001')
})
